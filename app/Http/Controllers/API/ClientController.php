<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Client;
//add models here

class ClientController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $clients = DB::table('clients')->where('', '%'.$queryw.'%')
						->orWhere('client_name', 'like', '%'.$queryw.'%')
						->orWhere('client_image', 'like', '%'.$queryw.'%')
						->orWhere('client_desc', 'like', '%'.$queryw.'%')
						->orWhere('date', 'like', '%'.$queryw.'%')->paginate(30);

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $clients = DB::table('clients')->where('', $queryw)->first();
            if ($clients){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $clients], 200);
            }

        }
        else
        $clients = Client::paginate(30);

        if (count($clients) > 0){
            return response()->json(['status' => 'success', 'count' => count($clients), 'data' => $clients], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($clients), 'message' => 'Failed! no clients found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['client_name'] = $data['client_name'];
		$saveData['client_image'] = $data['client_image'];
		$saveData['client_desc'] = $data['client_desc'];
		$saveData['date'] = $data['date'];

        $clients = Client::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $clients], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['client_name'] = $data['client_name'];
		$saveData['client_image'] = $data['client_image'];
		$saveData['client_desc'] = $data['client_desc'];
		$saveData['date'] = $data['date'];

        $row = Client::where('', $id)->first();
        if ($row){
            $Client = Client::where('', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $Client], 200);
    }

    public function delete(Request $request)
    {
        $delete = Client::where('', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
