<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\AlbumMedia;
//add models here

class AlbumMediaController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $albummedia = DB::table('albummedia')->where('', '%'.$queryw.'%')
						->orWhere('album_id', 'like', '%'.$queryw.'%')
						->orWhere('image_location', 'like', '%'.$queryw.'%')
						->orWhere('description', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')
						->orWhere('position', 'like', '%'.$queryw.'%')->paginate(30);

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $albummedia = DB::table('albummedia')->where('', $queryw)->first();
            if ($albummedia){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $albummedia], 200);
            }

        }
        else
        $albummedia = AlbumMedia::paginate(30);

        if (count($albummedia) > 0){
            return response()->json(['status' => 'success', 'count' => count($albummedia), 'data' => $albummedia], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($albummedia), 'message' => 'Failed! no albummedia found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['album_id'] = $data['album_id'];
		$saveData['image_location'] = $data['image_location'];
		$saveData['description'] = $data['description'];
		$saveData['status'] = $data['status'];
		$saveData['position'] = $data['position'];

        $albummedia = AlbumMedia::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $albummedia], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['album_id'] = $data['album_id'];
		$saveData['image_location'] = $data['image_location'];
		$saveData['description'] = $data['description'];
		$saveData['status'] = $data['status'];
		$saveData['position'] = $data['position'];

        $row = AlbumMedia::where('', $id)->first();
        if ($row){
            $AlbumMedia = AlbumMedia::where('', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $AlbumMedia], 200);
    }

    public function delete(Request $request)
    {
        $delete = AlbumMedia::where('', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
