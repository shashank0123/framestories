<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Album;
//add models here

class AlbumController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $albums = DB::table('albums')->where('', '%'.$queryw.'%')
						->orWhere('album_name', 'like', '%'.$queryw.'%')
						->orWhere('desc', 'like', '%'.$queryw.'%')
						->orWhere('client_id', 'like', '%'.$queryw.'%')
						->orWhere('album_cover', 'like', '%'.$queryw.'%')->paginate(30);

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $albums = DB::table('albums')->where('', $queryw)->first();
            if ($albums){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $albums], 200);
            }

        }
        else
        $albums = Album::paginate(30);

        if (count($albums) > 0){
            return response()->json(['status' => 'success', 'count' => count($albums), 'data' => $albums], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($albums), 'message' => 'Failed! no albums found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['album_name'] = $data['album_name'];
		$saveData['desc'] = $data['desc'];
		$saveData['client_id'] = $data['client_id'];
		$saveData['album_cover'] = $data['album_cover'];

        $albums = Album::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $albums], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['album_name'] = $data['album_name'];
		$saveData['desc'] = $data['desc'];
		$saveData['client_id'] = $data['client_id'];
		$saveData['album_cover'] = $data['album_cover'];

        $row = Album::where('', $id)->first();
        if ($row){
            $Album = Album::where('', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $Album], 200);
    }

    public function delete(Request $request)
    {
        $delete = Album::where('', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
