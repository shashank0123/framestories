<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Media;
//add models here

class MediaController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $media = DB::table('media')->where('', '%'.$queryw.'%')
						->orWhere('image_name', 'like', '%'.$queryw.'%')
						->orWhere('alt_text', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')
						->orWhere('specs', 'like', '%'.$queryw.'%')
						->orWhere('size', 'like', '%'.$queryw.'%')->paginate(30);

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $media = DB::table('media')->where('', $queryw)->first();
            if ($media){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $media], 200);
            }

        }
        else
        $media = Media::paginate(30);

        if (count($media) > 0){
            return response()->json(['status' => 'success', 'count' => count($media), 'data' => $media], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($media), 'message' => 'Failed! no media found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['image_name'] = $data['image_name'];
		$saveData['alt_text'] = $data['alt_text'];
		$saveData['status'] = $data['status'];
		$saveData['specs'] = $data['specs'];
		$saveData['size'] = $data['size'];

        $media = Media::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $media], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['image_name'] = $data['image_name'];
		$saveData['alt_text'] = $data['alt_text'];
		$saveData['status'] = $data['status'];
		$saveData['specs'] = $data['specs'];
		$saveData['size'] = $data['size'];

        $row = Media::where('', $id)->first();
        if ($row){
            $Media = Media::where('', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $Media], 200);
    }

    public function delete(Request $request)
    {
        $delete = Media::where('', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
