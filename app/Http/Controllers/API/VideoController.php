<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Video;
//add models here

class VideoController extends Controller
{
    public function index(Request $request){
        if (isset($request->q) && $request->q != 'undefined'){
            $queryw = $request->q;
            $videos = DB::table('videos')->where('', '%'.$queryw.'%')
						->orWhere('video_name', 'like', '%'.$queryw.'%')
						->orWhere('alt', 'like', '%'.$queryw.'%')
						->orWhere('desc', 'like', '%'.$queryw.'%')
						->orWhere('status', 'like', '%'.$queryw.'%')
						->orWhere('specs', 'like', '%'.$queryw.'%')
						->orWhere('size', 'like', '%'.$queryw.'%')->paginate(30);

        }
        elseif (isset($request->item_id)){
            $queryw = $request->item_id;
            $videos = DB::table('videos')->where('', $queryw)->first();
            if ($videos){
                return response()->json(['status' => 'success', 'count' => 1, 'data' => $videos], 200);
            }

        }
        else
        $videos = Video::paginate(30);

        if (count($videos) > 0){
            return response()->json(['status' => 'success', 'count' => count($videos), 'data' => $videos], 200);
        }
        else
            return response()->json(['status' => 'failed', 'count' => count($videos), 'message' => 'Failed! no videos found'], 200);
    }

    
    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['video_name'] = $data['video_name'];
		$saveData['alt'] = $data['alt'];
		$saveData['desc'] = $data['desc'];
		$saveData['status'] = $data['status'];
		$saveData['specs'] = $data['specs'];
		$saveData['size'] = $data['size'];

        $videos = Video::create($saveData);

        return response()->json(['status' => 'success', 'message' => 'Data added Successfully', 'data' => $videos], 200);
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['video_name'] = $data['video_name'];
		$saveData['alt'] = $data['alt'];
		$saveData['desc'] = $data['desc'];
		$saveData['status'] = $data['status'];
		$saveData['specs'] = $data['specs'];
		$saveData['size'] = $data['size'];

        $row = Video::where('', $id)->first();
        if ($row){
            $Video = Video::where('', $id)->update($saveData);
        }

        return response()->json(['status' => 'success', 'message' => 'Data updated Successfully', 'data' => $Video], 200);
    }

    public function delete(Request $request)
    {
        $delete = Video::where('', $request->id)->delete();
        return response()->json(['status' => 'success', 'message' => 'Data deleted Successfully', 'data' => $delete], 200);

    }
}
