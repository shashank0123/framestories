<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlbumMedia extends Model
{
    use HasFactory;
    protected $primaryKey = '';
    protected $table = 'albummedia';
    public $timestamps = false;
    protected $fillable = [
			'album_id',
			'image_location',
			'description',
			'status',
			'position',
		];
}
