<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use HasFactory;
    protected $primaryKey = '';
    protected $table = 'media';
    public $timestamps = false;
    protected $fillable = [
			'image_name',
			'alt_text',
			'status',
			'specs',
			'size',
		];
}
