<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    use HasFactory;
    protected $primaryKey = '';
    protected $table = 'albums';
    public $timestamps = false;
    protected $fillable = [
			'album_name',
			'desc',
			'client_id',
			'album_cover',
		];
}
