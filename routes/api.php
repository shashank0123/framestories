<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminRegisterController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\API\VideoController;
use App\Http\Controllers\API\MediaController;
use App\Http\Controllers\API\ClientController;
use App\Http\Controllers\API\AlbumMediaController;
use App\Http\Controllers\ReactBaseController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [RegisterController::class, 'login']);
Route::post('admin/login', [AdminRegisterController::class, 'login']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/videos', [VideoController::class, 'index']);
Route::post('/videos/add', [VideoController::class, 'store']);
Route::post('/videos/edit/{id}', [VideoController::class, 'update']);
Route::get('/videos/delete/{id}', [VideoController::class, 'delete']);
Route::get('/media', [MediaController::class, 'index']);
Route::post('/media/add', [MediaController::class, 'store']);
Route::post('/media/edit/{id}', [MediaController::class, 'update']);
Route::get('/media/delete/{id}', [MediaController::class, 'delete']);
Route::get('/clients', [ClientController::class, 'index']);
Route::post('/clients/add', [ClientController::class, 'store']);
Route::post('/clients/edit/{id}', [ClientController::class, 'update']);
Route::get('/clients/delete/{id}', [ClientController::class, 'delete']);
Route::get('/albummedia', [AlbumMediaController::class, 'index']);
Route::post('/albummedia/add', [AlbumMediaController::class, 'store']);
Route::post('/albummedia/edit/{id}', [AlbumMediaController::class, 'update']);
Route::get('/albummedia/delete/{id}', [AlbumMediaController::class, 'delete']);
Route::get('/albums', [AlbumController::class, 'index']);
Route::post('/albums/add', [AlbumController::class, 'store']);
Route::post('/albums/edit/{id}', [AlbumController::class, 'update']);
Route::get('/albums/delete/{id}', [AlbumController::class, 'delete']);
Route::get('menu', [ReactBaseController::class , 'getMenu']);
