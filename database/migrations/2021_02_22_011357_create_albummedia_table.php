<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlbumMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('albummedia', function (Blueprint $table) {
            $table->id('id');
			$table->string('album_id')->nullable();
			$table->string('image_location')->nullable();
			$table->string('description')->nullable();
			$table->string('status')->nullable();
			$table->string('position')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('albummedia');
    }
}
