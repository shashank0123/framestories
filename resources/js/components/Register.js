import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom'
import { connect } from 'react-redux';
import { register } from '../redux/actions/register';
import { checkToken } from '../redux/actions/checkToken';



class Register extends Component {



    constructor(props){
        super(props)
        let history = createHistory();
        this.state={
            username : '',
            password : ''
        }
        this.props.checkToken().then((result) => {
           if (this.props.checkTokens){
            //    go to dashboard
            history.push("/admin/dashboard");
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
           }
           else{
            //    stay on this page

           }
        })
        .catch((err) => {
            console.log(err)
        })
    }

    submitLogin(){
        this.props.register(this.state).then((result) => {
            console.log(this.props.logins)
            localStorage.setItem('token', this.props.logins.token)
            localStorage.setItem('name', this.props.logins.data.name)
            localStorage.setItem('email', this.props.logins.data.email)
        }).catch((err) => {

        });
    }

    render(){
    return (
        <div className="d-flex align-items-center justify-content-center bg-sl-primary ht-100v">

      <div className="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white">
        <div className="signin-logo tx-center tx-24 tx-bold tx-inverse">Backstage <span className="tx-info tx-normal">admin</span></div>
        <div className="tx-center mg-b-60">High Functionality Admin Panel</div>



        <div className="form-group">
          <input type="text" value={this.state.name} onChange={(e) => {this.setState({name : e.target.value})} } className="form-control" placeholder="Enter your username"/>
        </div>
        <div className="form-group">
          <input type="email" value={this.state.email} onChange={(e) => {this.setState({email : e.target.value})} } className="form-control" placeholder="Enter your Email"/>
        </div>
        <div className="form-group">
          <input type="text" value={this.state.phone} onChange={(e) => {this.setState({phone : e.target.value})} } className="form-control" placeholder="Enter your Mobile No"/>
        </div>

        <div className="form-group">
          <input type="password" value={this.state.password} onChange={(e) => {this.setState({password : e.target.value})} }  className="form-control" placeholder="Enter your password"/>
        </div>

        <div className="form-group">
            <input id="password-confirm" value={this.state.password_confirmation} onChange={(e) => {this.setState({password_confirmation : e.target.value})} }  placeholder="Password Confirmation" type="password" className="form-control" name="password_confirmation" required />
        </div>


        <button type="submit" onClick={() => {this.submitLogin()}} className="btn btn-info btn-block">Sign In</button>

        <div className="mg-t-60 tx-center">Already A Member? <a href="login" className="tx-info">Sign In</a></div>
      </div>
    </div>
    );
    }
}

const mapStateToProps = (state) => {
    return {
      registers: state.register.register,
      checkTokens: state.checkToken.checkToken,
    };
  };

  export default connect(mapStateToProps, {
    register,checkToken
  })(Register);

