import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import AdminHeader from './admin/layouts/AdminHeader'

class Header extends Component {

    render(){

        if (this.props.type == 'admin'){
            return (<AdminHeader/>)
        }
        else if (this.props.type == 'user'){
            return (
                <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Header Component for {this.props.type}</div>

                        <div className="card-body">I'm an header component!</div>
                    </div>
                </div>
            </div>
        </div>
            );
        }
        else{
            return (<></>);
        }


    }
}

export default Header;

