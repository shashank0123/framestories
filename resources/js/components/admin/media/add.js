import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_media } from '../../../redux/actions/media';
import { save_media } from '../../../redux/actions/media';
import { get_media } from '../../../redux/actions/media';
const createHistory = require('history').createBrowserHistory;

class ModifyMedia extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        image_name : '',alt_text : '',status : '',specs : '',size : '',query: ''
    }

    this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_media('token', this.state.id).then(() => {
                this.setState({item : this.props.get_medias});
                this.setState({editpage : 1});
                				this.setState({ image_name : this.props.get_medias.image_name }) 
				this.setState({ alt_text : this.props.get_medias.alt_text }) 
				this.setState({ status : this.props.get_medias.status }) 
				this.setState({ specs : this.props.get_medias.specs }) 
				this.setState({ size : this.props.get_medias.size }) 

                })
            console.log(this.props.get_medias, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
    }


    

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_media(this.state).then(()=> {
            console.log(this.props.save_medias)
            history.push('/admin/media');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_media(this.state).then(()=> {
            console.log(this.props.save_medias)
            history.push('/admin/media');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        

    render(){
        // console.log(this.state.size)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="form-group">
                <label >Image name</label>
                <input type="file" className="form-control-file" />
              </div>
              <div className="mb-3">
              <label  className="form-label">Alt text</label>
              <input type="text" value={this.state.alt_text} onChange={(e) => {this.setState({alt_text: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={this.handlestatusChange} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            <div className="mb-3">
              <label  className="form-label">Specs</label>
              <input type="text" value={this.state.specs} onChange={(e) => {this.setState({specs: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Size</label>
              <input type="text" value={this.state.size} onChange={(e) => {this.setState({size: e.target.value})}} className="form-control"/>
            </div>
            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {save_medias : state.media.save_media, 
        update_medias : state.media.update_media,
        get_medias : state.media.get_media,
    };
  };

  export default connect(mapStateToProps, {
     save_media, update_media, get_media
  })(ModifyMedia);


