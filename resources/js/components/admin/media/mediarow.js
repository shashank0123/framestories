import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_media } from '../../../redux/actions/media';

class MediaRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_media(id, 'token').then(()=>{console.log(this.props.delete_medias)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/media/edit/'+this.state.row.
                var url2 = '/admin/media/delete/'+this.state.row.
                return (<tr>
                <td>{this.state.row.image_name}</td>
<td>{this.state.row.alt_text}</td>
<td>{this.state.row.status}</td>
<td>{this.state.row.specs}</td>
<td>{this.state.row.size}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_medias : state.media.delete_media, 
    };
  };

  export default connect(mapStateToProps, {
     delete_media
  })(MediaRow);
