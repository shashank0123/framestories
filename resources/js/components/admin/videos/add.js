import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_videos } from '../../../redux/actions/videos';
import { save_videos } from '../../../redux/actions/videos';
import { get_videos } from '../../../redux/actions/videos';
const createHistory = require('history').createBrowserHistory;

class ModifyVideo extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        video_name : '',alt : '',desc : '',status : '',specs : '',size : '',query: ''
    }

    this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_videos('token', this.state.id).then(() => {
                this.setState({item : this.props.get_videoss});
                this.setState({editpage : 1});
                				this.setState({ video_name : this.props.get_videoss.video_name }) 
				this.setState({ alt : this.props.get_videoss.alt }) 
				this.setState({ desc : this.props.get_videoss.desc }) 
				this.setState({ status : this.props.get_videoss.status }) 
				this.setState({ specs : this.props.get_videoss.specs }) 
				this.setState({ size : this.props.get_videoss.size }) 

                })
            console.log(this.props.get_videoss, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
    }


    

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_videos(this.state).then(()=> {
            console.log(this.props.save_videoss)
            history.push('/admin/videos');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_videos(this.state).then(()=> {
            console.log(this.props.save_videoss)
            history.push('/admin/videos');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        

    render(){
        // console.log(this.state.size)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="form-group">
                <label >Video name</label>
                <input type="file" className="form-control-file" />
              </div>
              <div className="mb-3">
              <label  className="form-label">Alt</label>
              <input type="text" value={this.state.alt} onChange={(e) => {this.setState({alt: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                                <label >Desc</label>
                                <textarea value={this.state.desc}  onChange={(e) => {this.setState({desc: e.target.value})}}  className="form-control" id="validationTextarea">{this.state.$value}</textarea>
                              </div>
                              <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={this.handlestatusChange} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            <div className="mb-3">
              <label  className="form-label">Specs</label>
              <input type="text" value={this.state.specs} onChange={(e) => {this.setState({specs: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Size</label>
              <input type="text" value={this.state.size} onChange={(e) => {this.setState({size: e.target.value})}} className="form-control"/>
            </div>
            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {save_videoss : state.videos.save_videos, 
        update_videoss : state.videos.update_videos,
        get_videoss : state.videos.get_videos,
    };
  };

  export default connect(mapStateToProps, {
     save_videos, update_videos, get_videos
  })(ModifyVideo);


