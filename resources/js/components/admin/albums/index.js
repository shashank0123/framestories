import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { view_albums } from '../../../redux/actions//albums';
import { Link } from 'react-router-dom';
const createHistory = require('history').createBrowserHistory;
import AlbumRow from './albumsrow';

class ViewAlbum extends Component {

    state = {
        query : ''
    }
    constructor(props){
        super(props)
        this.checkLogin()
        this.getData()


    }

    checkLogin(){
        var token = localStorage.getItem('token')
        if (!token || token == undefined || token == 'undefined'){
            let history = createHistory();
            console.log('yaha aaya to hai')
            history.push('/admin/login');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;

        }
        console.log(token)
    }

    getData(){
        if (this.state.query == undefined){
            this.state.query = '';
        }
        this.props.view_albums(this.state).then(()=>{

            })
    }

    searchText(e){
        this.setState({query : e.target.value})
        this.getData();
        this.renderTableData()
    }

    renderTableData = () => {
        var data = this.props.view_albumss;
        if (data != undefined){
            var tabledata = data.map(function(element){
                return <AlbumRow datavalue={element}/>
                
            })
            return  <>{ tabledata }</>
        }
        else{
            return <tr><td>No Data Found</td></tr>
        }
    }

    render(){
        return (
        <>
        <div className='row pull-right'>
                        <div className='mb-3'>
                            <input type='text' className='form-control' value={this.state.query} onChange={(e) => {this.searchText(e)} }/>
                          </div>
                        <div className='col-md-3 mb-3'>
                            <a className='btn btn-primary' href='/admin/albums/add'>Add New</a>
                          </div>
                        </div>
                   
        <div className='container'>
            <table className='table bordered'>
            <thead>
                <tr>
                <td>Album Name</td>
<td>Desc</td>
<td>Client Id</td>
<td>Album Cover</td>
<td>Actions</td></tr>
                </thead>
                <tbody>
                
                {this.renderTableData()}
                </tbody>
                        
            </table>
            
        </div>
        </>
            )


    }
}

const mapStateToProps = (state) => {
    return {
      view_albumss: state.albums.view_albums,
    };
  };

  export default connect(mapStateToProps, {
    view_albums
  })(ViewAlbum);
