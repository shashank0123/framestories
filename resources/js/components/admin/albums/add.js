import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_albums } from '../../../redux/actions/albums';
import { save_albums } from '../../../redux/actions/albums';
import { get_albums } from '../../../redux/actions/albums';
const createHistory = require('history').createBrowserHistory;
import { view_clients } from '../../../redux/actions/clients'; 

class ModifyAlbum extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        album_name : '',desc : '',client_id : '',album_cover : '',clients : [],query: ''
    }

    this.handleclient_idChange = this.handleclient_idChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_albums('token', this.state.id).then(() => {
                this.setState({item : this.props.get_albumss});
                this.setState({editpage : 1});
                				this.setState({ album_name : this.props.get_albumss.album_name }) 
				this.setState({ desc : this.props.get_albumss.desc }) 
				this.setState({ client_id : this.props.get_albumss.client_id }) 
				this.setState({ album_cover : this.props.get_albumss.album_cover }) 

                })
            console.log(this.props.get_albumss, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
            

            this.props.view_clients(this.state).then(()=> {
                    if (this.props.view_clientss != undefined)
                        this.setState({ clients  : this.props.view_clientss })    

                    
                })
        
    }


    renderOptionclients(){
            var returnclients = <></>;

            
                    returnclients =  this.state.clients.map(function(element){
                        return <option key={element.client_id} value={element.client_id}>{element.client_name}</option>
                        })        
               
            return <>{returnclients}</>
        }

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_albums(this.state).then(()=> {
            console.log(this.props.save_albumss)
            history.push('/admin/albums');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_albums(this.state).then(()=> {
            console.log(this.props.save_albumss)
            history.push('/admin/albums');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handleclient_idChange(e){
            this.setState({ client_id: e.target.value });
        }
        handleclient_idChange(e){
            this.setState({ client_id: e.target.value });
        }
        

    render(){
        // console.log(this.state.album_cover)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
              <label  className="form-label">Album name</label>
              <input type="text" value={this.state.album_name} onChange={(e) => {this.setState({album_name: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                                <label >Desc</label>
                                <textarea value={this.state.desc}  onChange={(e) => {this.setState({desc: e.target.value})}}  className="form-control" id="validationTextarea">{this.state.$value}</textarea>
                              </div>
                              <div className="mb-3">
                              <label  className="form-label">Client id</label>
                              <select onChange={this.handleclient_idChange} value={this.state.client_id} className="form-control" >
                                  <option>Select Client id</option>
                                  {this.renderOptionclients()}
                                  
                                </select>
                            </div>
                            <div className="form-group">
                <label >Album cover</label>
                <input type="file" className="form-control-file" />
              </div>
              
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {view_clients: state.clients.view_clients,save_albumss : state.albums.save_albums, 
        update_albumss : state.albums.update_albums,
        get_albumss : state.albums.get_albums,
    };
  };

  export default connect(mapStateToProps, {
    view_clients, save_albums, update_albums, get_albums
  })(ModifyAlbum);


