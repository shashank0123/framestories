import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_albums } from '../../../redux/actions/albums';

class AlbumRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_albums(id, 'token').then(()=>{console.log(this.props.delete_albumss)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/albums/edit/'+this.state.row.
                var url2 = '/admin/albums/delete/'+this.state.row.
                return (<tr>
                <td>{this.state.row.album_name}</td>
<td>{this.state.row.desc}</td>
<td>{this.state.row.client_id}</td>
<td>{this.state.row.album_cover}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_albumss : state.albums.delete_albums, 
    };
  };

  export default connect(mapStateToProps, {
     delete_albums
  })(AlbumRow);
