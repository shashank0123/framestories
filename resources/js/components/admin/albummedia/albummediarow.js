import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_albummedia } from '../../../redux/actions/albummedia';

class AlbumMediaRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_albummedia(id, 'token').then(()=>{console.log(this.props.delete_albummedias)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/albummedia/edit/'+this.state.row.
                var url2 = '/admin/albummedia/delete/'+this.state.row.
                return (<tr>
                <td>{this.state.row.album_id}</td>
<td>{this.state.row.image_location}</td>
<td>{this.state.row.description}</td>
<td>{this.state.row.status}</td>
<td>{this.state.row.position}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_albummedias : state.albummedia.delete_albummedia, 
    };
  };

  export default connect(mapStateToProps, {
     delete_albummedia
  })(AlbumMediaRow);
