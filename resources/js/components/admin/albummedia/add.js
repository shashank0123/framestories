import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_albummedia } from '../../../redux/actions/albummedia';
import { save_albummedia } from '../../../redux/actions/albummedia';
import { get_albummedia } from '../../../redux/actions/albummedia';
const createHistory = require('history').createBrowserHistory;
import { view_albums } from '../../../redux/actions/albums'; 

class ModifyAlbumMedia extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        album_id : '',image_location : '',description : '',status : '',position : '',albums : [],query: ''
    }

    this.handlealbum_idChange = this.handlealbum_idChange.bind(this)
            this.handlestatusChange = this.handlestatusChange.bind(this)
            
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_albummedia('token', this.state.id).then(() => {
                this.setState({item : this.props.get_albummedias});
                this.setState({editpage : 1});
                				this.setState({ album_id : this.props.get_albummedias.album_id }) 
				this.setState({ image_location : this.props.get_albummedias.image_location }) 
				this.setState({ description : this.props.get_albummedias.description }) 
				this.setState({ status : this.props.get_albummedias.status }) 
				this.setState({ position : this.props.get_albummedias.position }) 

                })
            console.log(this.props.get_albummedias, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
            

            this.props.view_albums(this.state).then(()=> {
                    if (this.props.view_albumss != undefined)
                        this.setState({ albums  : this.props.view_albumss })    

                    
                })
        
    }


    renderOptionalbums(){
            var returnalbums = <></>;

            
                    returnalbums =  this.state.albums.map(function(element){
                        return <option key={element.album_id} value={element.album_id}>{element.album_name}</option>
                        })        
               
            return <>{returnalbums}</>
        }

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_albummedia(this.state).then(()=> {
            console.log(this.props.save_albummedias)
            history.push('/admin/albummedia');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_albummedia(this.state).then(()=> {
            console.log(this.props.save_albummedias)
            history.push('/admin/albummedia');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    handlealbum_idChange(e){
            this.setState({ album_id: e.target.value });
        }
        handlestatusChange(e){
            this.setState({ status: e.target.value });
        }
        handlealbum_idChange(e){
            this.setState({ album_id: e.target.value });
        }
        

    render(){
        // console.log(this.state.position)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
                              <label  className="form-label">Album id</label>
                              <select onChange={this.handlealbum_idChange} value={this.state.album_id} className="form-control" >
                                  <option>Select Album id</option>
                                  {this.renderOptionalbums()}
                                  
                                </select>
                            </div>
                            <div className="mb-3">
              <label  className="form-label">Image location</label>
              <input type="text" value={this.state.image_location} onChange={(e) => {this.setState({image_location: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
                                <label >Description</label>
                                <textarea value={this.state.description}  onChange={(e) => {this.setState({description: e.target.value})}}  className="form-control" id="validationTextarea">{this.state.$value}</textarea>
                              </div>
                              <div className="mb-3">
                              <label  className="form-label">Status</label>
                              <select onChange={this.handlestatusChange} value={this.state.status} className="form-control">
                              
                                  <option>Select Status</option><option value='Active'>Active</option>
                                  <option value='Deactive'>Deactive</option>
                                  </select>
                            </div>
                            <div className="mb-3">
              <label  className="form-label">Position</label>
              <input type="text" value={this.state.position} onChange={(e) => {this.setState({position: e.target.value})}} className="form-control"/>
            </div>
            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {view_albums: state.albums.view_albums,save_albummedias : state.albummedia.save_albummedia, 
        update_albummedias : state.albummedia.update_albummedia,
        get_albummedias : state.albummedia.get_albummedia,
    };
  };

  export default connect(mapStateToProps, {
    view_albums, save_albummedia, update_albummedia, get_albummedia
  })(ModifyAlbumMedia);


