import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { update_clients } from '../../../redux/actions/clients';
import { save_clients } from '../../../redux/actions/clients';
import { get_clients } from '../../../redux/actions/clients';
const createHistory = require('history').createBrowserHistory;

class ModifyClient extends Component {

    

    constructor(props){
        super(props)
        this.state = {
            item : {},
            id : '',
            editpage : 0,
        client_name : '',client_image : '',client_desc : '',date : '',query: ''
    }

    
        this.handleSubmit = this.handleSubmit.bind(this);
        this.callDependencies();
        this.getToken()
        this.getSingleItem()
    }

    getSingleItem(){
        var currenturl = window.location.href;
        if (currenturl.includes('/edit/')){
            var idarray = currenturl.split('/edit/')
            this.state.id = idarray[1]
            this.props.get_clients('token', this.state.id).then(() => {
                this.setState({item : this.props.get_clientss});
                this.setState({editpage : 1});
                				this.setState({ client_name : this.props.get_clientss.client_name }) 
				this.setState({ client_image : this.props.get_clientss.client_image }) 
				this.setState({ client_desc : this.props.get_clientss.client_desc }) 
				this.setState({ date : this.props.get_clientss.date }) 

                })
            console.log(this.props.get_clientss, 'state');
        }
    }

    async getToken(){
        this.setState({token: localStorage.getItem('token')})
    }

    callDependencies(){
        
    }


    

    handleSubmit(event) {
    event.preventDefault();
    console.log('yaha aaya tha')
    let history = createHistory();
        // this is in case of save
    if (this.state.editpage){
        this.props.update_clients(this.state).then(()=> {
            console.log(this.props.save_clientss)
            history.push('/admin/clients');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
    }
    else
        this.props.save_clients(this.state).then(()=> {
            console.log(this.props.save_clientss)
            history.push('/admin/clients');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
            })
  }

    

    render(){
        // console.log(this.state.date)
        return (
        <>
        <form onSubmit={this.handleSubmit}>
        
        <div className="mb-3">
              <label  className="form-label">Client name</label>
              <input type="text" value={this.state.client_name} onChange={(e) => {this.setState({client_name: e.target.value})}} className="form-control"/>
            </div>
            <div className="form-group">
                <label >Client image</label>
                <input type="file" className="form-control-file" />
              </div>
              <div className="mb-3">
              <label  className="form-label">Client desc</label>
              <input type="text" value={this.state.client_desc} onChange={(e) => {this.setState({client_desc: e.target.value})}} className="form-control"/>
            </div>
            <div className="mb-3">
              <label  className="form-label">Date</label>
              <input type="text" value={this.state.date} onChange={(e) => {this.setState({date: e.target.value})}} className="form-control"/>
            </div>
            
  <button type='submit' className='btn btn-primary'>Submit</button>
  </form>
</>
            )


    }
}
const mapStateToProps = (state) => {
    return {save_clientss : state.clients.save_clients, 
        update_clientss : state.clients.update_clients,
        get_clientss : state.clients.get_clients,
    };
  };

  export default connect(mapStateToProps, {
     save_clients, update_clients, get_clients
  })(ModifyClient);


