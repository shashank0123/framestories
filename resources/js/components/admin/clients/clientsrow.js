import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delete_clients } from '../../../redux/actions/clients';

class ClientRow extends Component {

    
    constructor(props){
        super(props)
        this.state = {row : this.props.datavalue, query : ''}
    }
    deleteRow = (id) => {
        console.log(id + ' now working')
        this.props.delete_clients(id, 'token').then(()=>{console.log(this.props.delete_clientss)})
        window.location.reload();
    }
    

    render(){
        
                var url1 = '/admin/clients/edit/'+this.state.row.
                var url2 = '/admin/clients/delete/'+this.state.row.
                return (<tr>
                <td>{this.state.row.client_name}</td>
<td>{this.state.row.client_image}</td>
<td>{this.state.row.client_desc}</td>
<td>{this.state.row.date}</td>
<td><Link to={url1}><i className='fa fa-pencil'></i></Link>   <span onClick={ () => this.deleteRow(this.state.row.)}><i className='fa fa-trash'></i></span></td></tr>)
    }
}



  const mapStateToProps = (state) => {
    return {
        delete_clientss : state.clients.delete_clients, 
    };
  };

  export default connect(mapStateToProps, {
     delete_clients
  })(ClientRow);
