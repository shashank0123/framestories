import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { view_clients } from '../../../redux/actions//clients';
import { Link } from 'react-router-dom';
const createHistory = require('history').createBrowserHistory;
import ClientRow from './clientsrow';

class ViewClient extends Component {

    state = {
        query : ''
    }
    constructor(props){
        super(props)
        this.checkLogin()
        this.getData()


    }

    checkLogin(){
        var token = localStorage.getItem('token')
        if (!token || token == undefined || token == 'undefined'){
            let history = createHistory();
            console.log('yaha aaya to hai')
            history.push('/admin/login');
            let pathUrl = window.location.href;
            window.location.href = pathUrl;

        }
        console.log(token)
    }

    getData(){
        if (this.state.query == undefined){
            this.state.query = '';
        }
        this.props.view_clients(this.state).then(()=>{

            })
    }

    searchText(e){
        this.setState({query : e.target.value})
        this.getData();
        this.renderTableData()
    }

    renderTableData = () => {
        var data = this.props.view_clientss;
        if (data != undefined){
            var tabledata = data.map(function(element){
                return <ClientRow datavalue={element}/>
                
            })
            return  <>{ tabledata }</>
        }
        else{
            return <tr><td>No Data Found</td></tr>
        }
    }

    render(){
        return (
        <>
        <div className='row pull-right'>
                        <div className='mb-3'>
                            <input type='text' className='form-control' value={this.state.query} onChange={(e) => {this.searchText(e)} }/>
                          </div>
                        <div className='col-md-3 mb-3'>
                            <a className='btn btn-primary' href='/admin/clients/add'>Add New</a>
                          </div>
                        </div>
                   
        <div className='container'>
            <table className='table bordered'>
            <thead>
                <tr>
                <td>Client Name</td>
<td>Client Image</td>
<td>Client Desc</td>
<td>Date</td>
<td>Actions</td></tr>
                </thead>
                <tbody>
                
                {this.renderTableData()}
                </tbody>
                        
            </table>
            
        </div>
        </>
            )


    }
}

const mapStateToProps = (state) => {
    return {
      view_clientss: state.clients.view_clients,
    };
  };

  export default connect(mapStateToProps, {
    view_clients
  })(ViewClient);
