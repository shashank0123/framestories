import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { menu } from '../../../../redux/actions/menu';
import { NavLink } from 'react-router-dom'

class AdminSideBar extends Component {

    constructor(props){
        super(props)
        this.state = {
            login : false
        }

    }


    createMenu(){


return  <></>
    }

    render(){
        if (localStorage.getItem('token'))
            
            return (
                <>
    <div className="sl-logo">
        <a href="">
            <i className="icon ion-android-star-outline"></i> FrameStories</a>
            </div>
    <div className="sl-sideleft">
      <div className="input-group input-group-search">
        <input type="search" name="search" className="form-control" placeholder="Search"/>
        <span className="input-group-btn">
          <button className="btn"><i className="fa fa-search"></i></button>
        </span>
      </div>

      <label className="sidebar-label">Navigation</label>
      <div className="sl-sideleft-menu">
        <a href="/admin/dashboard" className="sl-menu-link active">
          <div className="sl-menu-item">
            <i className="menu-item-icon icon ion-ios-home-outline tx-22"></i>
            <span className="menu-item-label">Dashboard</span>
          </div>
        </a>

        {this.createMenu()}
        <NavLink to='/admin/albums' className='sl-menu-link' activeClassName='active'>
            <div className='sl-menu-item'>
              <i className='menu-item-icon ion-ios-pie-outline tx-20'></i>
              <span className='menu-item-label'>Album</span>
              <i className='menu-item-arrow fa fa-angle-down'></i>
            </div>
          </NavLink>
          <ul className='sl-menu-sub nav flex-column'>
            <li className='nav-item'><NavLink to={'/admin/albums'+'/add'} className='nav-link'>Add Album</NavLink></li>
            <li className='nav-item'><NavLink to='/admin/albums' className='nav-link'>View Album</NavLink>
            </li>
          </ul>
        <NavLink to='/admin/albummedia' className='sl-menu-link' activeClassName='active'>
            <div className='sl-menu-item'>
              <i className='menu-item-icon ion-ios-pie-outline tx-20'></i>
              <span className='menu-item-label'>Album Media</span>
              <i className='menu-item-arrow fa fa-angle-down'></i>
            </div>
          </NavLink>
          <ul className='sl-menu-sub nav flex-column'>
            <li className='nav-item'><NavLink to={'/admin/albummedia'+'/add'} className='nav-link'>Add Album Media</NavLink></li>
            <li className='nav-item'><NavLink to='/admin/albummedia' className='nav-link'>View Album Media</NavLink>
            </li>
          </ul>
        <NavLink to='/admin/clients' className='sl-menu-link' activeClassName='active'>
            <div className='sl-menu-item'>
              <i className='menu-item-icon ion-ios-pie-outline tx-20'></i>
              <span className='menu-item-label'>Client</span>
              <i className='menu-item-arrow fa fa-angle-down'></i>
            </div>
          </NavLink>
          <ul className='sl-menu-sub nav flex-column'>
            <li className='nav-item'><NavLink to={'/admin/clients'+'/add'} className='nav-link'>Add Client</NavLink></li>
            <li className='nav-item'><NavLink to='/admin/clients' className='nav-link'>View Client</NavLink>
            </li>
          </ul>
        <NavLink to='/admin/media' className='sl-menu-link' activeClassName='active'>
            <div className='sl-menu-item'>
              <i className='menu-item-icon ion-ios-pie-outline tx-20'></i>
              <span className='menu-item-label'>Media</span>
              <i className='menu-item-arrow fa fa-angle-down'></i>
            </div>
          </NavLink>
          <ul className='sl-menu-sub nav flex-column'>
            <li className='nav-item'><NavLink to={'/admin/media'+'/add'} className='nav-link'>Add Media</NavLink></li>
            <li className='nav-item'><NavLink to='/admin/media' className='nav-link'>View Media</NavLink>
            </li>
          </ul>
        <NavLink to='/admin/videos' className='sl-menu-link' activeClassName='active'>
            <div className='sl-menu-item'>
              <i className='menu-item-icon ion-ios-pie-outline tx-20'></i>
              <span className='menu-item-label'>Video</span>
              <i className='menu-item-arrow fa fa-angle-down'></i>
            </div>
          </NavLink>
          <ul className='sl-menu-sub nav flex-column'>
            <li className='nav-item'><NavLink to={'/admin/videos'+'/add'} className='nav-link'>Add Video</NavLink></li>
            <li className='nav-item'><NavLink to='/admin/videos' className='nav-link'>View Video</NavLink>
            </li>
          </ul>
        
      </div>

      <br/>
    </div>
    </>
            );
            
            else
            return (<></>)

    }
}

const mapStateToProps = (state) => {
    return {
      menus: state.menu.menu,
    };
  };

  export default connect(mapStateToProps, {
    menu
  })(AdminSideBar);
