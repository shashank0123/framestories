import React, {Component} from 'react';
import ReactDOM from 'react-dom';

class AdminFooter extends Component {


    render(){

            return (
                <footer className="sl-footer">
            <div className="footer-left">
              <div className="mg-b-2">Copyright &copy; 2021. Farmbers. All Rights Reserved.</div>
              <div>Made by Farmbers.</div>
            </div>
            <div className="footer-right d-flex align-items-center">
              <span className="tx-uppercase mg-r-10">Share:</span>
              <a target="_blank" className="pd-x-5" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//www.farmbers.com/"><i className="fa fa-facebook tx-20"></i></a>
              <a target="_blank" className="pd-x-5" href="https://twitter.com/home?status=Farmbers%20now%20liveat%20http%3A//www.farmbers.com/"><i className="fa fa-twitter tx-20"></i></a>
            </div>
          </footer>
            );

    }
}

export default AdminFooter;
