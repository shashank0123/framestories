import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom'

class Home extends Component {

    render(){
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Welcome to the best website template</div>
                        <Link to={'/login'}>Login</Link>

                    </div>
                </div>
            </div>
        </div>
    );
    }
}

export default Home;
