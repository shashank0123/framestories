import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom'
import { connect } from 'react-redux';
import { login } from '../redux/actions/login';
import { checkToken } from '../redux/actions/checkToken';
import { result } from 'lodash';
const createHistory = require("history").createBrowserHistory;



class Login extends Component {



    constructor(props){
        super(props)
        this.state={
            email : '',
            password : ''
        }
        let history = createHistory();
        this.props.checkToken().then((result) => {
           if (this.props.checkTokens != '' && this.props.checkTokens != 'undefined'){
               console.log(this.props.checkTokens)
            //    go to dashboard
            history.push("/admin/dashboard");
            let pathUrl = window.location.href;
            window.location.href = pathUrl;
           }
           else{
            //    stay on this page

           }
        })
        .catch((err) => {
            console.log(err)
        })
    }

    submitLogin(){
        this.props.login(this.state).then((result) => {
            console.log(this.props.logins)
            localStorage.setItem('token', this.props.logins.token)
            localStorage.setItem('usertype', 'admin')
            localStorage.setItem('name', this.props.logins.data.name)
            localStorage.setItem('email', this.props.logins.data.email)
            let history = createHistory();
            history.push("/admin/dashboard");
            var newUrl = window.location.href;
            window.location.href = newUrl;

        }).catch((err) => {

        });
    }

    render(){

    return (
        <div className="d-flex align-items-center justify-content-center bg-sl-primary ht-100v">

      <div className="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white">
        <div className="signin-logo tx-center tx-24 tx-bold tx-inverse">Backstage <span className="tx-info tx-normal">admin</span></div>
        <div className="tx-center mg-b-60">High Functionality Admin Panel</div>

        <div className="form-group">
          <input type="text" className="form-control" value={this.state.email} onChange={(e) => {this.setState({email : e.target.value})} }/>
        </div>

        <div className="form-group">
          <input type="password" className="form-control" value={this.state.password} onChange={(e) => {this.setState({password : e.target.value})} } placeholder="Enter your password"/>
          <Link to={'forgot-password'} className="tx-info tx-12 d-block mg-t-10">Forgot password?</Link>
        </div>
        <button onClick={() => {this.submitLogin()}} className="btn btn-info btn-block">Sign In</button>

        <div className="mg-t-60 tx-center">Not yet a member? <a href="register" className="tx-info">Sign Up</a></div>
      </div>
    </div>
    );
    }
}

const mapStateToProps = (state) => {
    return {
      logins: state.login.login,
      checkTokens: state.checkToken.checkToken,
    };
  };

  export default connect(mapStateToProps, {
    login,checkToken
  })(Login);

