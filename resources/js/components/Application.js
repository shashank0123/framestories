import React, { Component, Suspense, lazy } from 'react';
import ReactDOM from 'react-dom';
import Header from './Header'
import Footer from './Footer'
import Navigation from '../Navigation'
import { Provider } from "react-redux"
import * as serviceWorker from "./serviceWorker"
import { store } from "../redux/store"
import UserRoot from './user/UserRoot';
import AdminHeader from './admin/layouts/AdminHeader';
import AdminFooter from './admin/layouts/AdminFooter';
import AdminSideBar from './admin/layouts/AdminSideBar';
import AdminRightSideBar from './admin/layouts/AdminRightSideBar';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import {BrowserRouter as Router} from 'react-router-dom'

class Application extends Component {



    constructor(props){
        super(props)
        this.state = {
            userType : 'admin',
        }
    }



    render(){
    return (
        <>
        <Provider store={store}>
      <Suspense fallback={<p>Loading...</p>}>
          {this.state.userType == 'admin' ? <>
          <Router>
            <AdminHeader/>
            <AdminSideBar/>
            <AdminRightSideBar/>
            <div className="sl-mainpanel">
                <div className="sl-pagebody">

                <Navigation/>
                </div>
                <AdminFooter/>
            </div>
            </Router>
        </> : <UserRoot/>}
      </Suspense>
    </Provider>



        </>
    );
    }
}

export default Application;

if (document.getElementById('app')) {
    ReactDOM.render(<Application />, document.getElementById('app'));
}
