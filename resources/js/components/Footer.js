import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import AdminFooter from './admin/layouts/AdminFooter/'

class Footer extends Component {


    render(){
        if (this.props.type == 'admin'){
            return (<AdminFooter/>);
        }
        else if (this.props.type == 'user'){
            return (
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-header">Footer Component  for {this.props.type}</div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        else{
            return (<></>);
        }
    }
}

export default Footer;
