import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import Home from '../components/Home'
import Login from '../components/Login'
import Register from '../components/Register'
import ForgotPassword from '../components/ForgotPassword'
import AdminDashboard from '../components/admin/layouts/AdminDashboard'
import NotFound from '../components/admin/layouts/NotFound'
import ViewVideo from '../components/admin/videos'
import ModifyVideo from '../components/admin/videos/add'
import ViewMedia from '../components/admin/media'
import ModifyMedia from '../components/admin/media/add'
import ViewClient from '../components/admin/clients'
import ModifyClient from '../components/admin/clients/add'
import ViewAlbumMedia from '../components/admin/albummedia'
import ModifyAlbumMedia from '../components/admin/albummedia/add'
import ViewAlbum from '../components/admin/albums'
import ModifyAlbum from '../components/admin/albums/add'
import Profile from '../components/admin/Profile'

class Navigation extends Component{

    constructor(props){
        super(props)
    }

 render() {



    return (

        <Switch>
        <Route path={'/'} exact component={Home}/>
        <Route path={'/login'} exact component={Login}/>
        <Route path={'/logout'} exact component={Login}/>
        <Route path={'/register'} exact component={Register}/>
        <Route path={'/forgot-password'} exact component={ForgotPassword}/>

        <Route path={'/admin/dashboard'} exact component={AdminDashboard}/>

        <Route path={'/admin/login'} exact component={Login}/>
        <Route path={'/admin/profile'} exact component={Profile}/>
        <Route path={'/admin/albums'} exact component={ViewAlbum}/>
        <Route path={'/admin/albums/add'} exact component={ModifyAlbum}/>
        <Route path={'/admin/albums/edit/:id'} exact component={ModifyAlbum}/>
        
        <Route path={'/admin/albummedia'} exact component={ViewAlbumMedia}/>
        <Route path={'/admin/albummedia/add'} exact component={ModifyAlbumMedia}/>
        <Route path={'/admin/albummedia/edit/:id'} exact component={ModifyAlbumMedia}/>
        
        <Route path={'/admin/clients'} exact component={ViewClient}/>
        <Route path={'/admin/clients/add'} exact component={ModifyClient}/>
        <Route path={'/admin/clients/edit/:id'} exact component={ModifyClient}/>
        
        <Route path={'/admin/media'} exact component={ViewMedia}/>
        <Route path={'/admin/media/add'} exact component={ModifyMedia}/>
        <Route path={'/admin/media/edit/:id'} exact component={ModifyMedia}/>
        
        <Route path={'/admin/videos'} exact component={ViewVideo}/>
        <Route path={'/admin/videos/add'} exact component={ModifyVideo}/>
        <Route path={'/admin/videos/edit/:id'} exact component={ModifyVideo}/>
        
        
        <Route>
            <NotFound />
        </Route>
        </Switch>

    );
}
}

export default Navigation;
