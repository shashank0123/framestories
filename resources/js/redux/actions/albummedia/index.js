import axios from 'axios';
import urlObj from '../../url';
export const view_albummedia = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
    var url = urlObj.albummedia_url+'?q='+state.query
		
    await axios
      .get(url)
      .then((response) => {
        
        dispatch({
          type: 'VIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_albummedia = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.albummedia_url+'/add?token='+state.token;
var data = new FormData()
data.append('album_id', state.album_id)
data.append('image_location', state.image_location)
data.append('description', state.description)
data.append('status', state.status)
data.append('position', state.position)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'SAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_albummedia = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.albummedia_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('album_id', state.album_id)
data.append('image_location', state.image_location)
data.append('description', state.description)
data.append('status', state.status)
data.append('position', state.position)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'EDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_albummedia = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.albummedia_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'DELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_albummedia = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.albummedia_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'GET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};