import axios from 'axios';
import url from './../../url';
export const register = (state) => {
  return async (dispatch, getState) => {
    await axios

      .post(url.register, state)
      .then((response) => {
        dispatch({
          type: 'REGISTER',
          payload: response.data.data,

        });

      })
      .catch((error) => {
        console.log(error);
      });
  };
};
