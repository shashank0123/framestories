import axios from 'axios';
import urlObj from '../../url';
export const view_videos = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
    var url = urlObj.videos_url+'?q='+state.query
		
    await axios
      .get(url)
      .then((response) => {
        
        dispatch({
          type: 'VIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_videos = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.videos_url+'/add?token='+state.token;
var data = new FormData()
data.append('video_name', state.video_name)
data.append('alt', state.alt)
data.append('desc', state.desc)
data.append('status', state.status)
data.append('specs', state.specs)
data.append('size', state.size)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'SAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_videos = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.videos_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('video_name', state.video_name)
data.append('alt', state.alt)
data.append('desc', state.desc)
data.append('status', state.status)
data.append('specs', state.specs)
data.append('size', state.size)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'EDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_videos = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.videos_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'DELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_videos = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.videos_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'GET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};