import axios from 'axios';
import urlObj from '../../url';
export const view_albums = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
    var url = urlObj.albums_url+'?q='+state.query
		
    await axios
      .get(url)
      .then((response) => {
        
        dispatch({
          type: 'VIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_albums = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.albums_url+'/add?token='+state.token;
var data = new FormData()
data.append('album_name', state.album_name)
data.append('desc', state.desc)
data.append('client_id', state.client_id)
data.append('album_cover', state.album_cover)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'SAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_albums = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.albums_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('album_name', state.album_name)
data.append('desc', state.desc)
data.append('client_id', state.client_id)
data.append('album_cover', state.album_cover)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'EDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_albums = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.albums_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'DELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_albums = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.albums_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'GET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};