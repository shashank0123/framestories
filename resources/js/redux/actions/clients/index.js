import axios from 'axios';
import urlObj from '../../url';
export const view_clients = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
    var url = urlObj.clients_url+'?q='+state.query
		
    await axios
      .get(url)
      .then((response) => {
        
        dispatch({
          type: 'VIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_clients = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.clients_url+'/add?token='+state.token;
var data = new FormData()
data.append('client_name', state.client_name)
data.append('client_image', state.client_image)
data.append('client_desc', state.client_desc)
data.append('date', state.date)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'SAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_clients = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.clients_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('client_name', state.client_name)
data.append('client_image', state.client_image)
data.append('client_desc', state.client_desc)
data.append('date', state.date)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'EDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_clients = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.clients_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'DELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_clients = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.clients_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'GET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};