import axios from 'axios';
import url from './../../url';
export const login = (state) => {
  return async (dispatch, getState) => {
    await axios

      .post(url.admin_login, state)
      .then((response) => {
        dispatch({
          type: 'LOGIN',
          payload: response.data,

        });

      })
      .catch((error) => {
        console.log(error);
      });
  };
};
