import axios from 'axios';
import urlObj from '../../url';
export const view_media = (state) => {
  return async (dispatch, getState) => {
        if (state.queryw == undefined){
            state.queryw = ''
        }
    var url = urlObj.media_url+'?q='+state.query
		
    await axios
      .get(url)
      .then((response) => {
        
        dispatch({
          type: 'VIEW',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const save_media = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.media_url+'/add?token='+state.token;
var data = new FormData()
data.append('image_name', state.image_name)
data.append('alt_text', state.alt_text)
data.append('status', state.status)
data.append('specs', state.specs)
data.append('size', state.size)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'SAVE',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const update_media = (state) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.media_url+'/edit/'+state.id+'?token='+state.token; 
var data = new FormData()
data.append('image_name', state.image_name)
data.append('alt_text', state.alt_text)
data.append('status', state.status)
data.append('specs', state.specs)
data.append('size', state.size)

		
    await axios
      .post(url, data)
      .then((response) => {
        dispatch({
          type: 'EDIT',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const delete_media = (id, token) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.media_url+'/delete/'+id+'?token='+token+'&id='+id; 

		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'DELETE',
          payload: response.data
          
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const get_media = (token, id) => {
  return async (dispatch, getState) => {
    
    var url = urlObj.media_url+'?token='+token+'&item_id='+id
		
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'GET',
          payload: response.data
          
        });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};