const initialState = {
    menu: []
  };

  export const menu = (state = initialState, action) => {
    switch (action.type) {
      case 'REGISTER': {
        return { ...state, menu: action.payload,getValue:'yes' };
      }
      default: {
        return state;
      }
    }
  };
  export default menu;
