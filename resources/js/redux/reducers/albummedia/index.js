const initialState = {
    view_albummedia: [],
    save_albummedia: {},
    update_albummedia: {},
    delete_albummedia: {},
    get_albummedia: {},
  };
  
  export const albummedia = (state = initialState, action) => {
    switch (action.type) {
      case 'VIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_albummedia:action.payload.data.data }
        }
      }
      case 'GET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_albummedia:action.payload.data }
        }
      }
      case 'SAVE': {
        return { ...state,save_albummedia:action.payload };
      }
      case 'EDIT': {
        return { ...state,update_albummedia:action.payload };
      }
      case 'DELETE': {
        return { ...state,delete_albummedia:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default albummedia;