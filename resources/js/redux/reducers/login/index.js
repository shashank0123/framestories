const initialState = {
    login: {}
  };

  export const login = (state = initialState, action) => {
    switch (action.type) {
      case 'LOGIN': {
        return { ...state, login: action.payload,getValue:'yes' };
      }
      default: {
        return state;
      }
    }
  };
  export default login;
