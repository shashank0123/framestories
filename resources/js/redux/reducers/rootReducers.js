import albums from "./albums"
import albummedia from "./albummedia"
import clients from "./clients"
import media from "./media"
import videos from "./videos"

import { combineReducers } from "redux"
// import customizer from "./customizer/"
import login from "./login"
import register from "./register"
import checkToken from "./checkToken"
import menu from "./menu"

const rootReducer = combineReducers({
  login:login,
  register:register,
  checkToken:checkToken,
  menu:menu

,
videos:videos
,
media:media
,
clients:clients
,
albummedia:albummedia
,
albums:albums
})

export default rootReducer
