const initialState = {
    view_videos: [],
    save_videos: {},
    update_videos: {},
    delete_videos: {},
    get_videos: {},
  };
  
  export const videos = (state = initialState, action) => {
    switch (action.type) {
      case 'VIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_videos:action.payload.data.data }
        }
      }
      case 'GET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_videos:action.payload.data }
        }
      }
      case 'SAVE': {
        return { ...state,save_videos:action.payload };
      }
      case 'EDIT': {
        return { ...state,update_videos:action.payload };
      }
      case 'DELETE': {
        return { ...state,delete_videos:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default videos;