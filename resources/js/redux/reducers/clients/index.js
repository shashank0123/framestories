const initialState = {
    view_clients: [],
    save_clients: {},
    update_clients: {},
    delete_clients: {},
    get_clients: {},
  };
  
  export const clients = (state = initialState, action) => {
    switch (action.type) {
      case 'VIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_clients:action.payload.data.data }
        }
      }
      case 'GET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_clients:action.payload.data }
        }
      }
      case 'SAVE': {
        return { ...state,save_clients:action.payload };
      }
      case 'EDIT': {
        return { ...state,update_clients:action.payload };
      }
      case 'DELETE': {
        return { ...state,delete_clients:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default clients;