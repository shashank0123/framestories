const initialState = {
    view_albums: [],
    save_albums: {},
    update_albums: {},
    delete_albums: {},
    get_albums: {},
  };
  
  export const albums = (state = initialState, action) => {
    switch (action.type) {
      case 'VIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_albums:action.payload.data.data }
        }
      }
      case 'GET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_albums:action.payload.data }
        }
      }
      case 'SAVE': {
        return { ...state,save_albums:action.payload };
      }
      case 'EDIT': {
        return { ...state,update_albums:action.payload };
      }
      case 'DELETE': {
        return { ...state,delete_albums:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default albums;