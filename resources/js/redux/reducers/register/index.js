const initialState = {
    register: {}
  };

  export const register = (state = initialState, action) => {
    switch (action.type) {
      case 'REGISTER': {
        return { ...state, register: action.payload,getValue:'yes' };
      }
      default: {
        return state;
      }
    }
  };
  export default register;
