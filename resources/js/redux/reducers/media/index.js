const initialState = {
    view_media: [],
    save_media: {},
    update_media: {},
    delete_media: {},
    get_media: {},
  };
  
  export const media = (state = initialState, action) => {
    switch (action.type) {
      case 'VIEW': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,view_media:action.payload.data.data }
        }
      }
      case 'GET': {
        if (action.payload.status == 'failed')
            return {state };
        else{
            return { ...state,get_media:action.payload.data }
        }
      }
      case 'SAVE': {
        return { ...state,save_media:action.payload };
      }
      case 'EDIT': {
        return { ...state,update_media:action.payload };
      }
      case 'DELETE': {
        return { ...state,delete_media:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default media;