var base_url = 'localhost:8001/api'
const urlObj = {
    admin_login: base_url+'/admin/login',
    register: base_url+'/register',
    menu: base_url+'/menu',
  

videos_url: base_url+"/videos",

media_url: base_url+"/media",

clients_url: base_url+"/clients",

albummedia_url: base_url+"/albummedia",

albums_url: base_url+"/albums",

};

  module.exports = urlObj;
