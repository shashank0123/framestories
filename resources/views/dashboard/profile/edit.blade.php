@extends('dashboard.layouts.dashboard')


@section('mainarea')

<form method="POST" action="{{ route('user.profile.update') }}" enctype="multipart/form-data">
  @csrf
  <div class="mb-3">
    <label for="userNameInput" class="form-label">User Name</label>
    <input type="text" name="name" value="{{$user->name}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
  </div>
  <div class="mb-3">
    <label for="Email" class="form-label">Email</label>
    <input type="email" value="{{$user->email}}" name="email" class="form-control" id="Email">
  </div>
  <div class="mb-3">
	  <label for="profilePic" class="form-label">Profile image</label>
	  <input class="form-control" name="profile_photo_path" type="file" id="profilePic">
	</div>
	<div class="mb-3">
		
	<img id="showImage" style="width: 100px;height: 100px" src="{{ (!empty($user->profile_photo_path))? url('uploads/user_image/'.$user->profile_photo_path): url('/uploads/noimage.jpg') }}">
	</div>
  <button type="submit" class="btn btn-primary">Update</button>
</form>


@endsection


@section('customscrips')

<script type="text/javascript">
  $(document).ready(function(){
    $('#profilePic').change(function(e){
      var reader = new FileReader();
      reader.onload = function(e){
        $('#showImage').attr('src', e.target.result);
      }
      reader.readAsDataURL(e.target.files['0'])
    });
  });
</script>


@endsection