<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Backstage</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="{{ asset('/admin/lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
        <link href="{{ asset('/admin/lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">
        <link href="{{ asset('/admin/lib/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet">
        <link href="{{ asset('/admin/lib/rickshaw/rickshaw.min.css')}}" rel="stylesheet">
        <script src="{{asset('js/app.js')}}" defer></script>
        <link rel="stylesheet" href="{{asset('css/starlight.css')}}">
    </head>
    <body>
        <div id="app"></div>


        </div><!-- sl-mainpanel -->
        <!-- ########## END: MAIN PANEL ########## -->
    <script src="{{ asset('/admin/lib/jquery/jquery.js') }}"></script>
    <script src="{{ asset('/admin/lib/popper.js/popper.js') }}"></script>
    <script src="{{ asset('/admin/lib/jquery-ui/jquery-ui.js') }}"></script>
    <script src="{{ asset('/admin/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
    <script src="{{ asset('/admin/lib/jquery.sparkline.bower/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('/admin/lib/d3/d3.js') }}"></script>
    <script src="{{ asset('/admin/lib/rickshaw/rickshaw.min.js') }}"></script>
    <script src="{{ asset('/admin/lib/chart.js/Chart.js') }}"></script>
    <script src="{{ asset('/admin/lib/Flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('/admin/lib/Flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('/admin/lib/Flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('/admin/lib/flot-spline/jquery.flot.spline.js') }}"></script>

    <script src="{{ asset('/admin/js/starlight.js') }}"></script>
    <script src="{{ asset('/admin/js/ResizeSensor.js') }}"></script>
    <script src="{{ asset('/admin/js/dashboard.js') }}"></script>
  </body>
</html>

