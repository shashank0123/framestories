<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Frame Stories</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Frame Stories">
  <meta name="author" content="Frame Stories">

  <!-- FAVICON -->
  <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">
<link rel="manifest" href="/assets/favicon/site.webmanifest">

  <!-- CSS -->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/css/animate.min.css">
  <link rel="stylesheet" href="assets/css/responsive.css">
  <link rel="stylesheet" href="assets/css/jquery.bxslider.css">
  <link rel="stylesheet" href="assets/css/jPushMenu.css">
  <link rel="stylesheet" href="assets/css/lightbox.css">
  <link rel="stylesheet" href="assets/css/main.css">

 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->

</head> <!-- head -->
<style>
.slider1{
    background-image: url(images1/image12.jpg);
    background-position: center;
}
.slider2{
    background-image: url(images1/image2.jpg);
    background-position: center;

}
.slider3{
    background-image: url(images1/image14.jpg);
    background-position: center;
}

@media only screen and (max-width: 600px) {
    .slider1{
        background-image: url(images1/image20.jpg);
        background-position: center;
    }
    .slider2{
        background-image: url(images1/image22.jpeg);
        background-position: center;
        background-position-x: -80px;
    }
    .slider3{
        background-image: url(images1/image4.jpg);
        background-position: center;
    }
}
</style>
<body>

	<!-- ============== start wrapper  ============== -->
	<div id="wrapper">
		<!-- main-nav -->
		<div class="main-nav">
			<div class="logo-left">
				<a data-scroll href="#home">
					<img style="width: 74px;margin-top: -7px;" src="logo.png" alt="logo">
				</a>
			</div>
			<div class="menu-button toggle-menu menu-right push-body">
				<button><i class="fa fa-bars"></i></button>
			</div>
		</div> <!-- //end main-nav -->

		<!--=========================
			Start area for Menu
		============================== -->
		<nav id="main-navigation" class="nav-menu nav-menu-vertical nav-menu-right">
			<ul class="list-inline">
				<li class="current"><a href="#home">Home</a></li>
				<li><a href="#portfolio">Photography</a></li>
				<li><a href="#features">Wedding Films</a></li>
				<li><a href="#features">About Us</a></li>
				<li><a href="#features">Packages</a></li>
				<li><a href="#teams">The Team</a></li>
				<li><a href="#client-feedback">Reviews</a></li>
				<li><a href="#contact">Contact</a></li>
			</ul>
		</nav>

		<!-- End of menu area -->


		<!-- ============== start home section  ============== -->
		<section id="home">
			<div id="home-slider" class="carousel slide in" data-ride="carousel">
			  <!-- Indicators -->
			<ol class="carousel-indicators">
				<li class="active" data-slide-to="0" data-target="#home-slider"></li>
				<li data-slide-to="1" data-target="#home-slider" class=""></li>
				<li data-slide-to="2" data-target="#home-slider" class=""></li>
			</ol>

		      <div class="carousel-inner">
		        <div class="item active slider1">
		        	<div class="col-sm-offset-1 caption caption-wide">
		        	</div>
		        </div><!-- //item -->
		        <div class="item slider2" >
		        	<div class="col-sm-offset-1 caption caption-wide">
		        	</div>
		        </div><!-- //item -->
		        <div class="item slider3" >
		          <div class="col-sm-offset-1 caption">
		        	</div>
		        </div><!-- //item -->
		      </div>
		    </div>
		</section>
		<!-- ========= //End home section ========= -->


		<!-- ============== Start features section ============== -->
		<section id="features">
			<div class="container">
				<div class="row">

					<div class="col-md-4 feature-item">
						<div class="content wow fadeInDown" data-wow-delay=".15s">
							<p class="icon text-center"><i class="fa fa-camera-retro"></i></p>
							<h5 class="title text-center">Wedding Photography</h5>
							<p class="info">
								Lorem Ipsum is simply dummy text of the printing and typesetting industry.
							</p>
						</div>
					</div>
					<div class="col-md-4 feature-item">
						<div class="content wow fadeInDown" data-wow-delay=".35s">
							<p class="icon text-center"><i class="fa fa-male"></i></p>
							<h5 class="title text-center">Pre Wedding Shoot</h5>
							<p class="info">
								Lorem Ipsum is simply dummy text of the printing and typesetting industry.
						</div>
					</div>
					<div class="col-md-4 feature-item">
						<div class="content wow fadeInDown" data-wow-delay=".55s">
							<p class="icon text-center"><i class="fa fa-users"></i></p>
							<h5 class="title text-center">Destination Pre/Wedding</h5>
							<p class="info">
								Lorem Ipsum is simply dummy text of the printing and typesetting industry.
							</p>
						</div>
					</div>
				</div> <!-- //row -->
			</div> <!-- //container -->
		</section>
		<!-- ========= //End features section ========= -->


		<!-- ============== Start portfolio section ============== -->
		<section id="portfolio" class="mtb100">
			<div class="container">
				<h3 class="section-title wow fadeInDown">Our Portfolio</h3>
				<p class="section-info col-sm-8 col-sm-offset-2 wow fadeInDown" data-wow-delay=".25s">
					We do not only say that we can. we have a long trail of work behind
				</p>
				<div class="clearfix"></div>

				<div class="portfolio-content">
					<ul class="portfolio-filter text-center list-inline mtb40 text-uppercase">
						<li><a class="active" href="#" data-filter="*"> All</a></li>
						<li><a href="#" data-filter=".wedding">Wedding</a></li>
						<li><a href="#" data-filter=".festival">Pre Wedding</a></li>
						<li><a href="#" data-filter=".engagement">Engagement</a></li>
						<li><a href="#" data-filter=".birthday">Destination</a></li>
					</ul>
					<div class="portfolio-items">
						<div style="height: 271px;" class="col-lg-3 col-md-4 col-sm-6 portfolio-item festival wedding">
						 	<figure class="content wow fadeIn" data-wow-delay=".15s">
						 		<img src="images1/2.jpg" style="height: 98%" class="img-100p" alt="portfolio">
						 		<figcaption class="overflow-content-full">
						 			<p class="icon horizontal-vertical-center">
						 				<a data-lightbox="portfolio" href="images1/image2.jpg"><i class="fa fa-search-plus"></i></a>
						 			</p>
						 			<div class="caption text-center">
						 				<p class="title">John Donga</p>
						 				<p class="info">Lorem Ipsum is simply dummy text of the printing industry. </p>
						 			</div>
						 		</figcaption>
						 	</figure>
						 </div>
						 <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item festival birthday">
						 	<figure class="content wow fadeIn" data-wow-delay=".25s">
						 		<img src="images1/4.jpg" class="img-100p" alt="portfolio">
						 		<figcaption class="overflow-content-full">
						 			<p class="icon horizontal-vertical-center">
						 				<a data-lightbox="portfolio" href="images1/image4.jpg"><i class="fa fa-search-plus"></i></a>
						 			</p>
						 			<div class="caption text-center">
						 				<p class="title">John Donga</p>
						 				<p class="info">Lorem Ipsum is simply dummy text of the printing industry. </p>
						 			</div>
						 		</figcaption>
						 	</figure>
						 </div>
						 <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item engagement">
						 	<figure class="content wow fadeIn" data-wow-delay=".35s">
						 		<img src="images1/14.jpg" class="img-100p" alt="portfolio">
						 		<figcaption class="overflow-content-full">
						 			<p class="icon horizontal-vertical-center">
						 				<a data-lightbox="portfolio" href="images1/image14.jpg"><i class="fa fa-search-plus"></i></a>
						 			</p>
						 			<div class="caption text-center">
						 				<p class="title">John Donga</p>
						 				<p class="info">Lorem Ipsum is simply dummy text of the printing industry. </p>
						 			</div>
						 		</figcaption>
						 	</figure>
						 </div>
						 <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item birthday wedding hit-and-run">
						 	<figure class="content wow fadeIn" data-wow-delay=".45s">
						 		<img src="images1/17.jpg" class="img-100p" alt="portfolio">
						 		<figcaption class="overflow-content-full">
						 			<p class="icon horizontal-vertical-center">
						 				<a data-lightbox="portfolio" href="images1/image17.jpg"><i class="fa fa-search-plus"></i></a>
						 			</p>
						 			<div class="caption text-center">
						 				<p class="title">John Donga</p>
						 				<p class="info">Lorem Ipsum is simply dummy text of the printing industry. </p>
						 			</div>
						 		</figcaption>
						 	</figure>
						 </div>
						 <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item engagement birthday hit-and-run">
						 	<figure class="content wow fadeIn" data-wow-delay=".55s">
						 		<img src="images1/13.jpg" class="img-100p" alt="portfolio">
						 		<figcaption class="overflow-content-full">
						 			<p class="icon horizontal-vertical-center">
						 				<a data-lightbox="portfolio" href="images1/image13.jpg"><i class="fa fa-search-plus"></i></a>
						 			</p>
						 			<div class="caption text-center">
						 				<p class="title">John Donga</p>
						 				<p class="info">Lorem Ipsum is simply dummy text of the printing industry. </p>
						 			</div>
						 		</figcaption>
						 	</figure>
						 </div>
						 <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item festival wedding hit-and-run">
						 	<figure class="content wow fadeIn" data-wow-delay=".65s">
						 		<img src="images1/20.jpg" class="img-100p" alt="portfolio">
						 		<figcaption class="overflow-content-full">
						 			<p class="icon horizontal-vertical-center">
						 				<a data-lightbox="portfolio" href="images1/image20.jpg"><i class="fa fa-search-plus"></i></a>
						 			</p>
						 			<div class="caption text-center">
						 				<p class="title">John Donga</p>
						 				<p class="info">Lorem Ipsum is simply dummy text of the printing industry. </p>
						 			</div>
						 		</figcaption>
						 	</figure>
						 </div>
						 <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item festival wedding">
						 	<figure class="content wow fadeIn" data-wow-delay=".75s">
						 		<img src="images1/12.jpg" class="img-100p" alt="portfolio">
						 		<figcaption class="overflow-content-full">
						 			<p class="icon horizontal-vertical-center">
						 				<a data-lightbox="portfolio" href="images1/image12.jpg"><i class="fa fa-search-plus"></i></a>
						 			</p>
						 			<div class="caption text-center">
						 				<p class="title">John Donga</p>
						 				<p class="info">Lorem Ipsum is simply dummy text of the printing industry. </p>
						 			</div>
						 		</figcaption>
						 	</figure>
						 </div>
						 <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item engagement hit-and-run">
						 	<figure class="content wow fadeIn" data-wow-delay=".85s">
						 		<img src="images1/11.jpg" class="img-100p" alt="portfolio">
						 		<figcaption class="overflow-content-full">
						 			<p class="icon horizontal-vertical-center">
						 				<a data-lightbox="portfolio" href="images1/image11.jpg"><i class="fa fa-search-plus"></i></a>
						 			</p>
						 			<div class="caption text-center">
						 				<p class="title">John Donga</p>
						 				<p class="info">Lorem Ipsum is simply dummy text of the printing industry. </p>
						 			</div>
						 		</figcaption>
						 	</figure>
						 </div>
					</div>
				</div>
			</div> <!-- // container -->
		</section>
		<!-- ========= //End portfolio section ========= -->


		<!-- ============== Start counter ============== -->
		<section id="count-down" class="text-center">
			<div class="color-overlay ptb100">
				<div class="container">
					<h3 class="section-title wow fadeInDown">Notable Stats</h3>
					<div class="row">
						<div class="counter-content">
							<div class="col-sm-4 each-counter wow zoomIn">
								<span class="counter count1">2043</span>
								<p class="title">Events Covered</p>
							</div>
							<div class="col-sm-4 each-counter wow zoomIn">
								<span class="counter count2">1602</span>
								<p class="title">Weddings</p>
							</div>
							<div class="col-sm-4 each-counter wow zoomIn">
								<span class="counter count3">441</span>
								<p class="title">Birthday Parties</p>
							</div>
						</div>
					</div> <!-- //row -->
				</div> <!-- //container -->
			</div>
		</section>
		<!-- ========= //End counter ========= -->


		<!-- ============== Start team section  ============== -->
		<section id="teams" class="ptb100">
			<div class="container">
				<h3 class="section-title wow fadeInDown">The Best Team</h3>
				<p class="section-info col-sm-8 col-sm-offset-2 wow fadeInDown" data-wow-dealy=".25s">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry.
				</p>
				<div class="clearfix"></div>

				<div class="row">
					<div class="col-lg-3 col-sm-6 col-xs-12 team-member wow fadeInDown" data-wow-delay=".25s">
						<div class="content">
							<figure class="member-pic">
								<img src="img/team/01.jpg" alt="team-member" class="img-100p">
								<figcaption class="caption">
									<div class="color-overlay overflow-content-full">
										<ul class="social-icon horizontal-vertical-center list-inline">
											<li><a href="#"><i class="fa fa-facebook-official "></i></a></li>
											<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus-square "></i></a></li>
										</ul>
									</div>
								</figcaption>
							</figure>
							<div class="member-info">
								<p class="position">Wedding Photography Chief</p>
								<p class="name">John Dongi</p>
								<p>
									Lorem Ipsum is simply dummy text of the printing and typesetting industry.
								</p>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 col-xs-12 team-member wow fadeInDown" data-wow-delay=".45s">
						<div class="content">
							<figure class="member-pic">
								<img src="img/team/02.jpg" alt="team-member" class="img-100p">
								<figcaption class="caption">
									<div class="color-overlay overflow-content-full">
										<ul class="social-icon horizontal-vertical-center list-inline">
											<li><a href="#"><i class="fa fa-facebook-official "></i></a></li>
											<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus-square "></i></a></li>
										</ul>
									</div>
								</figcaption>
							</figure>
							<div class="member-info">
								<p class="position">Festival Photography Chief</p>
								<p class="name">John Donga</p>
								<p>
									Lorem Ipsum is simply dummy text of the printing and typesetting industry.
								</p>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 col-xs-12 team-member wow fadeInDown" data-wow-delay=".65s">
						<div class="content">
							<figure class="member-pic">
								<img src="img/team/03.jpg" alt="team-member" class="img-100p">
								<figcaption class="caption">
									<div class="color-overlay overflow-content-full">
										<ul class="social-icon horizontal-vertical-center list-inline">
											<li><a href="#"><i class="fa fa-facebook-official "></i></a></li>
											<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus-square "></i></a></li>
										</ul>
									</div>
								</figcaption>
							</figure>
							<div class="member-info">
								<p class="position">Ringingbell Designer</p>
								<p class="name">Johhny D.</p>
								<p>
									Lorem Ipsum is simply dummy text of the printing and typesetting industry.
								</p>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 col-xs-12 team-member wow fadeInDown" data-wow-delay=".85s">
						<div class="content">
							<figure class="member-pic">
								<img src="img/team/04.jpg" alt="team-member" class="img-100p">
								<figcaption class="caption">
									<div class="color-overlay overflow-content-full">
										<ul class="social-icon horizontal-vertical-center list-inline">
											<li><a href="#"><i class="fa fa-facebook-official "></i></a></li>
											<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus-square "></i></a></li>
										</ul>
									</div>
								</figcaption>
							</figure>
							<div class="member-info">
								<p class="position">Photography Template Developer</p>
								<p class="name">J. Dongian</p>
								<p>
									This week's free website template by Frittt is a photography website.
								</p>
							</div>
						</div>
					</div>
				</div> <!-- //row -->
			</div> <!-- //container -->
		</section>
		<!-- ========= //End team section  ========= -->


		<!-- ============== Start client-feedback ============== -->
		<section id="client-feedback">
			<div class="color-overlay ptb100">
				<div class="container">
					<h3 class="section-title wow fadeInDown">Happy Clients</h3>
					<div class="row">
						<div class="col-sm-10 col-sm-offset-1">
							<div class="custom-control">
								<p><span id="slider-prev"></span> <span id="slider-next"></span></p>
							</div>
							<ul class="bx-slider main-content">
								<li>
									<p>There is just something so fun about this one pager for the photography website template. The typography and load transitions are just awesome adding to the uplifting tone that all weddings aim to have.</p>
									<p class="client-name"><a href="#">J. Donga</a></p>
								</li>
								<li>
									<p>There is just something so fun about this one pager for the photography website template. The typography and load transitions are just awesome adding to the uplifting tone that all weddings aim to have.</p>
									<p class="client-name"><a href="#">D. John</a></p>
								</li>
								<li>
									<p>There is just something so fun about this one pager for the photography website template. The typography and load transitions are just awesome adding to the uplifting tone that all weddings aim to have.</p>
									<p class="client-name"><a href="#">S. Dongian</a></p>
								</li>
								<li>
									<p>There is just something so fun about this one pager for the photography website template. The typography and load transitions are just awesome adding to the uplifting tone that all weddings aim to have.</p>
									<p class="client-name"><a href="#">P. Donga</a></p>
								</li>
							</ul>
						</div>
					</div> <!-- //row -->
				</div> <!-- //container -->
			</div>
		</section>
		<!-- ========= //End client-feedback ========= -->





		<!-- ============== Start contact section ============== -->
		<section id="contact">
			<div class="map-content">
				<div id="gmap">
				</div>
			</div>
			<div class="color-overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-sm-offset-2 form-content ptb100">
							<h3 class="section-title wow fadeInDown">Impressed? Let's touch base</h3>
							<p class="section-info wow fadeInDown" data-wow-delay=".25s">
								Should you have any question or concern, you can reach us by filling out the contact form below or you can find us on other social networks.
							</p>
							<div class="clearfix"></div>
							<form action="#" class="form wow fadeIn" data-wow-delay=".25s">
								<input type="text" class="form-control" placeholder="Your Full Name">
								<input type="text" class="form-control" placeholder="Email Address">
								<input type="text" class="form-control" placeholder="Contact Number">
								<textarea name="" class="form-control" cols="30" rows="5" placeholder="Your Message"></textarea>
								<a class="btn-block btn btn-submit text-uppercase">Send Message Now</a>
							</form>
						</div>
					</div> <!-- //row -->
				</div> <!-- //container -->
			</div>
		</section>
		<!-- ========= //End contact section ========= -->


		<!-- ============== Start footer ============== -->
		<section id="footer">
			<div class="container bottom-part">
				<div class="row">
					<div class="col-sm-4 left-part">
						<p class="footer-logo "><img src="img/logo.png" alt="logo"></p><br/>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
					</div>
					<div class="col-sm-4 col-sm-offset-4">
						<ul class="social-icon-footer list-inline mtb50">
							<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
							<li><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
							<li><a href="#"><i class="fa fa-youtube-square"></i></a></li>
							<li><a href="#"><i class="fa fa-tumblr-square"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
						</ul>
						<form action="#" class="form subscribe-form">
							<p class="text-uppercase mtb20">get subscribed to the photography tips and news</p>
							<input type="email" class="form-control" placeholder="Enter your email:">
						</form>
					</div>
				</div> <!-- //row -->
			</div> <!-- //container -->

			<!-- copyright -->
			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-sm-offset-4">
							<p class="info">
								&copy; <a href="https://www.frittt.com/" target="_blank">Ringingbell</a>. Photography Website Template.
							</p>
						</div>
					</div> <!-- //row -->
				</div> <!-- //container -->
			</div> <!-- //end copyright -->
		</section>
		<!-- ========= //End footer ========= -->

	</div> <!-- //end wrapper -->

  <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
  <!-- loader -->
  <script src="{{ asset('assets/js/loader.min.js') }}"></script>
  <script src="{{ asset('assets/js/preloader.js') }}"></script>
  <!-- animation effects -->
  <script src="{{ asset('assets/js/wow.min.js') }}"></script>
  <!-- count-down -->
  <script src="{{ asset('assets/js/jquery.inview.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.counterup.min.js') }}"></script>
  <!-- content slider -->
  <script src="{{ asset('assets/js/jquery.bxslider.min.js') }}"></script>
  <!-- google maps -->
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  <script src="{{ asset('assets/js/gmaps.js') }}"></script>
  <script src="{{ asset('assets/js/init-map.js') }}"></script>
  <!-- pushmenu -->
  <script src="{{ asset('assets/js/jPushMenu.js') }}"></script>
  <!-- smooth navigation -->
  <script src="{{ asset('assets/js/mousescroll.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.nav.js') }}"></script>
  <!-- filter portfolio & lightbox -->
  <script src="{{ asset('assets/js/jquery.isotope.min.js') }}"></script>
  <script src="{{ asset('assets/js/lightbox.min.js') }}"></script>
  <!-- smooth scrool -->
  <script src="{{ asset('assets/js/smoothscroll.js') }}"></script>
  <!-- custom js -->
  <script src="{{ asset('assets/js/app.js') }}"></script>

</body>

</html>
